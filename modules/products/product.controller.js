const ProductModel = require('./product.model');

function map_product_req(product, productData) {
    if (productData.name)
        product.name = productData.name;
    if (productData.description)
        product.description = productData.description;
    if (productData.category)
        product.category = productData.category;
    if (productData.color)
        product.color = productData.color;
    if (productData.brand)
        product.brand = productData.brand;
    if (productData.price)
        product.price = productData.price;
    if (productData.quantity)
        product.quantity = productData.quantity;
    if (productData.images)
        product.images = productData.images;
    if (productData.status)
        product.status = productData.status;
    if (productData.tags)
        product.tags = typeof (productData.tags) === 'string'
            ? productData.tags.split(',')
            : productData.tags;
    if (productData.vendor)
        product.vendor = productData.vendor;
    // TODO handle boolean
    if (productData.warrentyStatus)
        product.warrentyStatus = productData.warrentyStatus;
    if (productData.warrentyPeroid)
        product.warrentyPeroid = productData.warrentyPeroid;

    // note flag must be in boolen
    if (productData.isReturnEligible == true || productData.isReturnEligible == false) {
        product.isReturnEligible = productData.isReturnEligible;
    }

    if (productData.salesDate)
        product.salesDate = productData.salesDate;
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate;
    if (productData.manuDate)
        product.manuDate = productData.manuDate;
    if (productData.expiryDate)
        product.expiryDate = productData.expiryDate;
    if (!product.discount)
        product.discount = {};
    // handle boolean value
    if (productData.discountedItem)
        product.discount.discountedItem = productData.discountedItem;
    if (productData.discountType)
        product.discount.discountType = productData.discountType;
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue;

}

function insert(req, res, next) {
    // parse incoming data,
    // validate data
    // prepare data
    // db operation
    // req-res cycle complete
    // console.log('req.file >>>', req.file);
    // console.log('req.files >>>', req.files);
    // console.log('req.body >>>', req.body);
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    const data = req.body;
    // prepare data
    data.vendor = req.user._id;
    if (req.files) {
        data.images = req.files.map(function (item, index) {
            return item.filename;
        })
    }
    const newProduct = new ProductModel({});
    // newProduct is mongoose object
    map_product_req(newProduct, data)
    newProduct
        .save()
        .then(function (saved) {
            res.json(saved);
        })
        .catch(function (err) {
            next(err);
        })



}

function find(req, res, next) {
    var condition = {};
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    // projection
    ProductModel
        .find(condition)
        // .sort()
        // .limit()
        // .skip()
        .populate('vendor', {
            username: 1,
            email: 1
        })
        // .populate('reviews.user') TODO
        .exec(function (err, products) {
            if (err) {
                return next(err);
            }
            res.json(products)
        })
}

function findById(req, res, next) {
    ProductModel.findById(req.params.id, function (err, product) {
        if (err) {
            return next(err);
        }
        if (!product) {
            return next({
                msg: 'Product Not Found',
                status: 404
            })
        }
        res.json(product);
    })
}

function update(req, res, next) {
    ProductModel.findById(req.params.id, function (err, product) {
        if (err) {
            return next(err)
        }
        if (!product) {
            return next({
                msg: "Product Not Found",
                status: 404
            })
        }
        // product found now update
        if (req.fileTypeError) {
            return next({
                msg: "Invalid File Format",
                status: 400
            })
        }

        const data = req.body;
        if (req.files) {
            data.images = req.files.map(function (item, index) {
                return item.filename;
            });
        }
        // data preparation
        map_product_req(product, data)

        if (data.reviewPoint && data.reviewMessage) {
            var reviewData = {};
            if (data.reviewPoint)
                reviewData.point = data.reviewPoint
            if (data.reviewMessage)
                reviewData.message = data.reviewMessage
            reviewData.user = req.user._id;

        }
        product.reviews.push(reviewData);

        product.save(function (err, updated) {
            if (err) {
                return next(err);
            }
            // TODO remove existing images form server
            res.json(updated)
        })

    })

}

function remove(req, res, next) {

    ProductModel.findById(req.params.id, function (err, product) {
        if (err) {
            return next(err);
        }
        if (!product) {
            return next({
                msg: "Product Not Found",
                status: 404
            })
        }
        product.remove(function (err, removed) {
            if (err) {
                return next(err);
            }
            res.json(removed)
        })

    })
}

function addReview(req, res, next) {
    ProductModel.findById(req.params.productId, function (err, product) {
        if (err) {
            return next(err)
        }
        if (!product) {
            return next({
                msg: "Product Not Found",
                status: 404
            })
        }
        // product found now update

        const data = req.body;
        // data preparation

        if (data.reviewPoint && data.reviewMessage) {
            var reviewData = {};
            if (data.reviewPoint)
                reviewData.point = data.reviewPoint
            if (data.reviewMessage)
                reviewData.message = data.reviewMessage
            reviewData.user = req.user._id;

        }
        product.reviews.push(reviewData);

        product.save(function (err, updated) {
            if (err) {
                return next(err);
            }
            res.json(updated)
        })

    })
}

function search(req, res, next) {
    var condition = {};
    // TODO search
    ProductModel
        .find(condition)
        .exec(function (err, products) {
            if (err) {
                return next(err);
            }
            res.json(products)
        })
}

module.exports = { // object shorthand
    insert,
    find,
    findById,
    update,
    remove,
    addReview,
    search
}
