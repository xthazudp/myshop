const router = require('express').Router();
const ProductController = require('./product.controller');

// middleware
// file upload middleware
const authenticate = require('../../middlewares/authenticate');
const uploader = require('./../../middlewares/uploader')

router.route('/')
    .get(authenticate, ProductController.find)
    .post(authenticate, uploader.array('image'), ProductController.insert);

router.route('/add-review/:productId')
    .post(authenticate, ProductController.addReview)

router.route('/search')
    .get(ProductController.search)
    .post(ProductController.search);

router.route('/:id')
    .get(authenticate, ProductController.findById)
    .put(authenticate, uploader.array('image'), ProductController.update)
    .delete(authenticate, ProductController.remove);


module.exports = router;
