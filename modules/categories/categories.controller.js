const CategoryModel = require("./categories.model");

function map_category_req(category, categoryData) {
  if (categoryData.cName) category.cName = categoryData.cName;
  if (categoryData.slug) category.slug = categoryData.slug;
  if (categoryData.parentId) category.parentId = categoryData.parentId;
}

function insert(req, res, next) {
  const data = req.body;

  const newCategory = new CategoryModel({});
  map_category_req(newCategory, data);
  newCategory
    .save()
    .then(function (saved) {
      res.json(saved);
    })
    .catch(function (err) {
      next(err);
    });
}

function createCategoryList(category, parentId = null) {
  const list = [];
  let categories;
  if (parentId == null) {
    categories = category.filter(function (cat) {
      return cat.parentId == undefined;
    });
    // categories = category.filter((cat) => cat.parentId == undefined);
  } else {
    categories = category.filter(function (cat) {
      return cat.parentId == parentId;
    });
    // categories = category.filter((cat) => cat.parentId == parentId);
  }
  for (let cate of categories) {
    list.push({
      _id: cate._id,
      cName: cate.cName,
      slug: cate.slug,
      subcategory: createCategoryList(category, cate._id),
    });
  }

  return list;
}

function find(req, res, next) {
  var condition = {};

  CategoryModel.find(condition).exec(function (err, category) {
    if (err) {
      return next(err);
    }
    const list = createCategoryList(category);
    res.json(list);
  });
}

function findById(req, res, next) {
  CategoryModel.findById(req.params.id, function (err, category) {
    if (err) {
      return next(err);
    }
    if (!category) {
      return next({
        msg: "category Not Found",
        status: 404,
      });
    }
    res.json(category);
  });
}

function remove(req, res, next) {
  CategoryModel.findById(req.params.id, function (err, category) {
    if (err) {
      return next(err);
    }
    if (!category) {
      return next({
        msg: "category Not Found",
        status: 404,
      });
    }
    category.remove(function (err, removed) {
      if (err) {
        return next(err);
      }
      res.json(removed);
    });
  });
}

function update(req, res, next) {
  CategoryModel.findById(req.params.id, function (err, category) {
    if (err) {
      return next(err);
    }
    if (!category) {
      return next({
        msg: "category Not Found",
        status: 404,
      });
    }
    // category found now update

    const data = req.body;
    // data preparation
    map_category_req(category, data);

    category.save(function (err, updated) {
      if (err) {
        return next(err);
      }
      res.json(updated);
    });
  });
}

module.exports = {
  insert,
  find,
  findById,
  remove,
  update,
};
