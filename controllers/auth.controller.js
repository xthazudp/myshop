const express = require('express');
const UserModel = require('./../models/user.model');
const router = express.Router(); // routing level middleware
const MAP_USER_REQ = require('./../helpers/map_user_request');
const Uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');

// js Docs commeting style

/**
 * create token with given data
 * @param {object} data 
 * @returns string
 */
function createToken(data) {
    let token = jwt.sign({
        _id: data._id
    }, config.JWT_SECRECT);
    return token;
}

router.post('/login', function (req, res, next) {

    UserModel
        .findOne({
            username: req.body.username
        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Invalid username",
                    status: 400
                })
            }
            if (user.status !== 'active') {
                return next({
                    msg: "Your Account is disabled please contact system administrator for support",
                    status: 401
                })
            }
            // if user
            var isMatched = passwordHash.verify(req.body.password, user.password)
            if (isMatched) {
                // token generation
                var token = createToken(user);
                res.json({
                    user: user,
                    token: token
                })
            }
            else {
                next({
                    msg: "Invalid Password",
                    status: 400
                })
            }
            // token generation
            // complete req-res cycle
        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/register', Uploader.single('image'), function (req, res, next) {
    // console.log('req.bdy. o>>>>', req.body);
    // console.log('req file >>', req.file)
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    if (req.file) {
        req.body.image = req.file.filename;
    }
    // if there is value in req. file our file is uploaded in server
    // data validation
    // password hashing
    // db operation
    // response

    var newUser = new UserModel({});
    //new user  is mongoose object
    // _id, --v,  default model , timestamps plugin
    // methods are function available to execute db query
    var newMappedUser = MAP_USER_REQ(newUser, req.body)
    newMappedUser.password = passwordHash.generate(req.body.password);

    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })


})

module.exports = router;
